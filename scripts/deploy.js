const hre = require("hardhat");
require("color");

async function main() {

  const ERC20 = await hre.ethers.getContractFactory("LuxuryShares");
  const erc20 = await ERC20.deploy("LuxuryShares", "LXS", "700000000000000000000000000");
  await erc20.deployed();
  console.log("Luxury Address:", erc20.address);

  
  const ERC20_ICO = await hre.ethers.getContractFactory("ERC20_ICO");
  const ico = await ERC20_ICO.deploy(erc20.address, "0x7ef95a0FEE0Dd31b22626fA2e10Ee6A223F8a684");  //0x55d398326f99059fF775485246999027B3197955
  await ico.deployed();
  console.log("ERC20-ICO Address:", ico.address);
  // console.log("Release time", releaseTime);
  console.log("verify command: npx hardhat verify " + ico.address + " " + erc20.address + " 0x7ef95a0FEE0Dd31b22626fA2e10Ee6A223F8a684");

  await erc20.setICOAddress(ico.address);
  await erc20.transfer(ico.address, "300000000000000000000000000");
  await ico.buy("2322000000000000000000000");

  await erc20.addLimitedGuy(["0x42F9dcd93DDCB82ED531A609774F6304275DeeaD"], 50);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
