// SPDX-License-Identifier: MIT

pragma solidity ^0.8.4;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./ERC20Capped.sol";
import "./ERC20Mintable.sol";
import "./ERC20Burnable.sol";
import "hardhat/console.sol";

/**
 * @title LuxuryShares
 * @dev Implementation of the ERC20
 */

interface IICO { 
    function isAlive() external view returns (bool);
}

contract LuxuryShares is ERC20Capped, ERC20Mintable, ERC20Burnable {
    uint256 public holderCount;
    IICO private icoContract;
    address public icoAddress = address(0);

    struct LimitInfo {
        uint256 startTime;
        uint256 lastUpdate;
        uint256 TotalAmount;
        uint256 maxSellAmount;
        uint256 validAmount;
    }

    address[] public limitedList;
    uint64 public limitedCount = 0;
    mapping(address => bool) public limitedGuy;
    mapping(address => LimitInfo) public limitedInfos;
    mapping(address => uint8) public sellLimit;
    address public sellAddress;
    uint256 public limitTime = 3600 * 24 * 30;

    constructor (string memory t_name, string memory t_symbol, uint256 t_cap)
        ERC20(t_name, t_symbol)
        ERC20Capped(t_cap)
        payable
    {
        _setupDecimals(18);
        holderCount = 0;
        sellAddress = address(0);
        _mint(msg.sender, t_cap);
    }

    /**
     * @dev Function to mint tokens.
     *
     * NOTE: restricting access to owner only. See {ERC20Mintable-mint}.
     *
     * @param account The address that will receive the minted tokens
     * @param amount The amount of tokens to mint
     */
    function _mint(address account, uint256 amount) internal override(ERC20, ERC20Capped) onlyOwner {
        require(amount > 0, "zero amount");
        uint256 am = balanceOf(account);
        super._mint(account, amount);

        if(am == 0)
            holderCount ++;
    }

    /**
     * @dev Function to stop minting new tokens.
     *
     * NOTE: restricting access to owner only. See {ERC20Mintable-finishMinting}.
     */
    function _finishMinting() internal override onlyOwner {
        super._finishMinting();
    }

    function setICOAddress(address addr) public onlyOwner{
        require(icoAddress == address(0), "Already Set");
        icoAddress = addr;
        icoContract = IICO(addr);
    }

    function setLimitTime(uint256 t) public onlyOwner {
        limitTime = t;
    }

    function getLastLimit(uint256 st) internal view returns(uint256){
        return st + ((block.timestamp - st) / limitTime) * limitTime;
    }

    function getLimitInfo(uint64 i) public view returns(address, uint256, uint256, uint256, uint256, uint256){
        require(i < limitedCount, "Out of data");
        address account = limitedList[i];
        LimitInfo memory details = limitedInfos[account];
        uint256 maxLimitAmount = details.maxSellAmount;
        uint256 validAmount = details.validAmount;
        uint256 balance = balanceOf(account);
        uint256 from = details.lastUpdate;
        uint256 to = from + limitTime;
        return (account, maxLimitAmount, validAmount, balance, from, to);
    }

    function addLimitedGuy(address[] memory accounts, uint8 percentage) public onlyOwner {
        for(uint8 i; i< accounts.length; i++) {
            limitedGuy[accounts[i]] = true;
            sellLimit[accounts[i]] = percentage;
            LimitInfo memory info = LimitInfo(
                block.timestamp,
                block.timestamp,
                balanceOf(accounts[i]),
                balanceOf(accounts[i]) * percentage / 100,
                balanceOf(accounts[i]) * percentage / 100
            );
            limitedInfos[accounts[i]] = info;
            limitedList.push(accounts[i]);
            limitedCount ++;
        }
    }

    function setLimitAmount(address account, uint256 amount) public onlyOwner {
        LimitInfo storage details = limitedInfos[account];
        uint256 valid = amount - (details.maxSellAmount - details.validAmount);
        if(details.lastUpdate != getLastLimit(details.startTime)) {
            valid = amount;
            details.lastUpdate = getLastLimit(details.startTime);
        }
        
        details.validAmount = valid;
        details.maxSellAmount = amount;
        console.log(details.validAmount);
    }

    function removeLimitedGuy(address account) public onlyOwner{
        limitedGuy[account] = false;
    }

    /**
     * @dev See {IERC20-transferFrom}.
     *
     * Emits an {Approval} event indicating the updated allowance. This is not
     * required by the EIP. See the note at the beginning of {ERC20}.
     * Take transaction fee from sender and transfer fee to the transaction fee wallet.
     *
     * Requirements:
     *
     * - `sender` and `recipient` cannot be the zero address.
     * - `sender` must have a balance of at least `amount`.
     * - the caller must have allowance for ``sender``'s tokens of at least
     * `amount`.
     */
    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        uint256 recipient_amount = balanceOf(recipient);
        if(sender != owner() && sender != icoAddress) {
            require(icoAddress != address(0), "ICO not setup");
            require(icoContract.isAlive(), "Transfer locked during ICO");    
        }

       if(limitedGuy[recipient]) {
            calcLimit(recipient);
        }

        if(limitedGuy[sender] && (sellAddress == address(0) || recipient == sellAddress)) {
            calcLimit(msg.sender);
            LimitInfo storage details = limitedInfos[sender];
            require(amount < details.validAmount, "Your transfer amount is limited");
            details.validAmount -= amount;
        }

        super.transferFrom(sender, recipient, amount);

        if(recipient_amount == 0)
            holderCount ++;
        if(balanceOf(sender) == 0)
            holderCount --;
        return true;
    }

    function calcLimit(address investor) internal {
        LimitInfo storage details = limitedInfos[investor];
        uint256 m = getLastLimit(details.startTime);
        if(m > details.lastUpdate) {
            details.lastUpdate = m;
            details.TotalAmount = balanceOf(investor);
            details.maxSellAmount = details.TotalAmount * sellLimit[investor] / 100;
            details.validAmount = details.maxSellAmount;
        }
    }

    /**
     * @dev See {IERC20-transfer}.
     *
     * Requirements:
     *
     * - `recipient` cannot be the zero address.
     * - the caller must have a balance of at least `amount`.
     */
    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        uint256 recipient_amount = balanceOf(recipient);

        if(msg.sender != owner() && msg.sender != icoAddress) {
            require(icoAddress != address(0), "ICO not setup");
            require(icoContract.isAlive(), "Transfer locked during ICO");    
        }
        
        if(limitedGuy[recipient]) {
            calcLimit(recipient);
        }

        if(limitedGuy[msg.sender] && (sellAddress == address(0) || recipient == sellAddress)) {
            calcLimit(msg.sender);
            LimitInfo storage details = limitedInfos[msg.sender];
            require(amount < details.validAmount, "Your transfer amount is limited");
            details.validAmount -= amount;
        }

        super.transfer(recipient, amount);

        if(recipient_amount == 0)
            holderCount ++;
        if(balanceOf(address(msg.sender)) == 0)
            holderCount --;
        return true;
    }
}