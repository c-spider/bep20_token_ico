const { expect } = require("chai");
const exp = require("constants");
const { ethers } = require("hardhat");
const hre = require("hardhat");
const util = require('util')

var factoryContract = null;
var erc20, ico;
var owner, addr1, addr2, addr3;


const timer = util.promisify(setTimeout);

describe("Test Token Contract", function () {
  it("Deploy Token", async function () {
    [owner, addr1, addr2, addr3] = await ethers.getSigners();
    owner_addr = owner.address;

    const ERC20 = await hre.ethers.getContractFactory("LuxuryShares");
    erc20 = await ERC20.deploy("LuxuryShares", "LXS", "700000000000000000000000000");
    await erc20.deployed();
  });

  it("Deploy ICO Contract", async function () {
    const ERC20_ICO = await hre.ethers.getContractFactory("ERC20_ICO");
    ico = await ERC20_ICO.deploy(erc20.address, "0x7ef95a0FEE0Dd31b22626fA2e10Ee6A223F8a684");  //0x55d398326f99059fF775485246999027B3197955
    await ico.deployed();
  });

  it("Config settings", async function () {
    await erc20.setICOAddress(ico.address);
    await erc20.transfer(ico.address, "300000000000000000000000000");
  })

  it("Buy from ICO Contract", async function() {
    await ico.buy("2322000000000000000000000");
    await erc20.transfer(addr1.address, "1000000000000000000000");
  })

  it("Regist investors in Smart Contract", async function() {
    await erc20.addLimitedGuy([addr1.address, addr2.address], 50);
    const data = await erc20.infos(addr1.address);
    const limit = await erc20.sellLimit(addr1.address);
    expect(limit).to.equal(50);
    expect(data.validAmount).equal("500000000000000000000");
  })

  it("Try to transfer token on Investors account : Limit Error", async function() {
    expect( erc20.connect(addr1).transfer(addr2.address, "700000000000000000000")).to.be.revertedWith("");
  }) 

  it("Try to transfer token on Investors account : success", async function() {
    await erc20.connect(addr1).transfer(addr2.address, "300000000000000000000");
    const data = await erc20.infos(addr1.address);
    expect(data.validAmount).equal("200000000000000000000");
    expect( erc20.connect(addr1).transfer(addr2.address, "300000000000000000000")).to.be.revertedWith("");
  }) 

  it("Set Limit for 1 account", async function() {
    await erc20.setLimitAmount(addr1.address, "800000000000000000000")
    await timer(20);
    const data = await erc20.infos(addr1.address);
    console.log(data);
    expect(data.maxSellAmount).equal("800000000000000000000");
    expect(data.validAmount).equal("500000000000000000000");
    await erc20.connect(addr1).transfer(addr2.address, "300000000000000000000");
    data = await erc20.infos(addr1.address);
    expect(data.validAmount).equal("200000000000000000000");
  }) 
});